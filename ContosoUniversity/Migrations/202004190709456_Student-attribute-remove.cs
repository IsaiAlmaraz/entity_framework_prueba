﻿namespace ContosoUniversity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Studentattributeremove : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Student", "Removed", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Student", "Removed");
        }
    }
}
